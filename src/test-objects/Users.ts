import { User } from 'src/app/components/models/user';

export const FireUser = {
  'uid': 'ZJ4B4pUT1ZVkI8tRP5fUwMjG28C2',
  'displayName': null,
  'photoURL': null,
  'email': 'nathangavenski@gmail.com',
  'emailVerified': false,
  'phoneNumber': null,
  'isAnonymous': false,
  'providerData': [{
    'uid': 'nathangavenski@gmail.com',
    'displayName': null,
    'photoURL': null,
    'email': 'nathangavenski@gmail.com',
    'phoneNumber': null,
    'providerId': 'password'
  }],
  'apiKey': 'AIzaSyC1T4xvelyDJmMJtXh3EN25awxMAIAuHCs',
  'appName': '[DEFAULT]',
  'authDomain': 'personal-blockchain.firebaseapp.com',
  'stsTokenManager': {
    'apiKey': 'AIzaSyC1T4xvelyDJmMJtXh3EN25awxMAIAuHCs',
    'refreshToken': 'AGK09AMmE2tU5BY-g4uiuEZWTxtxFVqxqoQdSVvJfhMvhy1mjQ6DzF5zwH4Hn-6UG9Jret-nGANeFyivkFqCOz4teurobotg9w9Pnqhb7zfiTxM0oq5GCMnZpDbMRPEyGw_40i_bVp-QuiZr_NHIqv_YRjo13USi_YGSO_KGeDVidnQQpsTKnVa23UkLrNsQTkiGzrr6iNM6viEbrQpVkDpUs7dMJUEmMeFY5NIxnrUeoSdEzX-ZUcE',
    'accessToken': 'eyJhbGciOiJSUzI1NiIsImtpZCI6IjIzNTBiNWY2NDM0Zjc2Y2NiM2IxMTlmZGQ4OGQxMzhjOWFjNTVmY2UiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vcGVyc29uYWwtYmxvY2tjaGFpbiIsImF1ZCI6InBlcnNvbmFsLWJsb2NrY2hhaW4iLCJhdXRoX3RpbWUiOjE1NDM3MDUzMDYsInVzZXJfaWQiOiJaSjRCNHBVVDFaVmtJOHRSUDVmVXdNakcyOEMyIiwic3ViIjoiWko0QjRwVVQxWlZrSTh0UlA1ZlV3TWpHMjhDMiIsImlhdCI6MTU0Mzc2OTY5NywiZXhwIjoxNTQzNzczMjk3LCJlbWFpbCI6Im5hdGhhbmdhdmVuc2tpQGdtYWlsLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwiZmlyZWJhc2UiOnsiaWRlbnRpdGllcyI6eyJlbWFpbCI6WyJuYXRoYW5nYXZlbnNraUBnbWFpbC5jb20iXX0sInNpZ25faW5fcHJvdmlkZXIiOiJwYXNzd29yZCJ9fQ.pFMuX2j3HwrbHmoqLCwNKTQxxmrMRkOFmeLRqrdNc0djqFPZmH53_feRfyI00FIpS92J5VfE0fzOaPP-F5uU2zbk8GQXcOb2PYmZYQDIjw294P_Apk8K3DWpepE-Lg5ROyGIlRQh8q-A1ozR5hXd84dgcULvjvlD6Q-i3vOG-rq5TsS5sbFxhUo9BrYvpFiou8tVLgS4qzZiX3Q8PAtZutp7BG-X0LXC8LVppzdKrbdawgaeFPZAg0g9xhmkBAsTbroKdJl8M11EzeBhCzyzRdV9XnLxduDJUOurNj0qrkQFPQJ9GWw73gpKE8e9-PP1huqmQgsgqd5Gl2BuAqKXAg',
    'expirationTime': 1543773283335
  },
  'redirectEventId': null,
  'lastLoginAt': '1543705306000',
  'createdAt': '1541523789000'
};

export const UserModel = new User(
  'nathangavenski@gmail.com', 'teste123', 'Nathan',
  'Gavenski', '51-993134034', '730512000',
  'N/A', '', 'Tv. Universina Araujo Nunes 72',
  '307', 'Porto Alegre', 'Rio Grande do Sul',
  91420526, ''
);
